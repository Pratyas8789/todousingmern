import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import axios from 'axios'
const userApi = "http://localhost:3007/user"

const initialState = {
    userName: "",
    password: "",
    userData: "",
    todoAcces: false,
}
export const getUserData = createAsyncThunk("userData", async () => {
    return axios.get(userApi)
        .catch((err) => console.log(err))
})

const userSlice = createSlice({
    name: "userData",
    initialState,
    reducers: {
        handleUserName: (state, { payload }) => {
            state.userName = payload
        },
        handleUserPassword: (state, { payload }) => {
            state.password = payload
        },
        handleTodoAccess: (state) => {
            state.todoAcces = !state.todoAcces
        },
        handleClear: (state) => {
            state.userName = "",
            state.password = ""
            localStorage.removeItem("token");
        }
    },
    extraReducers: {
        [getUserData.fulfilled]: (state, { payload }) => {
            state.userData = payload.data
        }
    }

})
export const { handleUserName, handleUserPassword, handleClear, handleTodoAccess } = userSlice.actions
export default userSlice.reducer