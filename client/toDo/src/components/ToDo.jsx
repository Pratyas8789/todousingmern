import React, { useEffect, useState } from 'react'
import Button from 'react-bootstrap/Button';
import { MdDelete } from 'react-icons/md';
import { useDispatch, useSelector } from 'react-redux';
import Modal from 'react-bootstrap/Modal';
import { Link, useParams } from 'react-router-dom';
import { handleClear } from '../Redux/Reducer/userReducer';

const API_BASE = "http://localhost:3007/user/todos"

export default function ToDo() {
  const { userName } = useSelector(store => store.userData)
  const dispatch = useDispatch()
  const [item, setItem] = useState([])

  const parms = useParams()
  const userId = parms.id
  const [newTodo, setNewTodo] = useState("")
  const [toogle, setToogle] = useState(false)

  // console.log(localStorage.token);
  useEffect(() => {
    fetch(API_BASE + "/" + userId, {
      headers: {
        "authorization": localStorage.token
      }
    })
      .then((res) => res.json())
      .then((data) => {
        setItem(data.todos)
      })
      .catch((err) => console.log(err))
  }, [])

  const handledelete = async (id) => {
    try {
      const res = await fetch(API_BASE + "/delete/" + id, {
        method: "DELETE",
         headers: {
          "authorization": localStorage.token
        }
      })
      const data = await res.json()
      setItem(todos => todos.filter(todo => todo._id !== data.todo._id))
    } catch (error) {
      console.log(error.message);
    }
  }

  let allTodos
  if (Array.isArray(item) && item.length > 0) {
    allTodos = item.map((eachItem) => {
      return (
        <div key={eachItem._id} className="board rounded d-flex justify-content-between align-items-center ps-2 pe-2 m-2 shadow-lg bg-white ">
          <div className='w-75 d-flex justify-content-between'>
            <h3>{eachItem.text}</h3>
            <h3>{eachItem.createdAt}</h3>
          </div>
          <div>
            <Button variant="white" onClick={() => handledelete(eachItem._id)} > <MdDelete style={{ color: "black" }} /></Button>{' '}
          </div>
        </div>
      )
    })
  }

  const handleChange = (todo) => {
    updateDebouncedText(todo)
  }

  const updateDebouncedText = debounce(todo => {
    fetch(API_BASE + "/" + userId + "/" + todo, {
      headers: {
        "authorization": localStorage.token
      }
    })
      .then((res) => res.json())
      .then((data) => {
        if (todo) {
          setItem(data.filterTodo)
        }
        else {
          setItem(data.todos)
        }
      })
      .catch((err) => console.log(err))
  }, 3000)

  function debounce(cb, delay = 5000) {
    let timeout
    return (todo) => {
      clearTimeout(timeout)
      timeout = setTimeout(() => {
        cb(todo)
      }, delay)
    }
  }

  const handletododata = async () => {
    try {
      if (newTodo !== "") {
        const data = await fetch(API_BASE, {
          method: "POST",
          headers: {
            "Content-type": "application/json",
            "authorization": localStorage.token
          },
          body: JSON.stringify({
            "text": newTodo,
            "userId": userId
          })
        }).then((res) => res.json())
        setItem([...item, data["newTodo"]])
        setNewTodo("")
        handleToogle()
      } else {
        alert("please enter a to-do name")
      }
    }
    catch (err) {
      console.log(err);
    }
  }

  const handleNewTodo = (todoItem) => {
    setNewTodo(todoItem)
  }

  const handleToogle = () => {
    setToogle(!toogle)
  }

  const handleDate = (day) => {
    let date
    if (day === "all-day") {
      date = 0
    } else if (day === "today") {
      let temDate = new Date()
      date = temDate.getDate()
    } else if (day === "yesterday") {
      let temDate = new Date()
      date = temDate.getDate() - 1
    }
    fetch(API_BASE + "/date/" + userId + "/" + date
    , {
      headers: {
        "authorization": localStorage.token
      }
    }
    )
      .then((res) => res.json())
      .then((data) => setItem(data.filterTodo))
      .catch((err) => console.log(err.message))
  }

  return (
    <div className='todoContainer d-flex flex-column align-items-center' >
      <div className='w-100 d-flex' >
        <div className='w-50 d-flex justify-content-end ' >
          <h1>ToDo</h1>
        </div>
        <div className='w-50 d-flex justify-content-end pe-2' >
          <Link to='/' >
            <Button className='m-2' onClick={() => dispatch(handleClear())} variant="danger">Log out</Button>{' '}
          </Link>

        </div>
      </div>
      <div className='w-100 d-flex h-100'>
        <div className='w-25 d-flex flex-column' >
          <div className='d-flex justify-content-around ' >
            <h3>welcome {userName} </h3>
          </div>
          <div className='d-flex justify-content-around'>
            <input className=' rounded border-0' type="text" onChange={() => handleChange(event.target.value)} name="" id="" placeholder='search todo' />
            <select onClick={() => handleDate(event.target.value)} name="" className='rounded border-0' id="">
              <option value="all-day">all-day</option>
              <option value="today">today</option>
              <option value="yesterday">yesterday</option>
            </select>
          </div>
        </div>
        <div className="w-75" >
          {allTodos}
          <Button className='ms-2' variant="primary" onClick={handleToogle}  >Add new todo</Button>{' '}
        </div>
      </div>
      {toogle &&
        <>
          <Modal show={toogle} onHide={handleToogle}>
            <Modal.Header closeButton>
              <Modal.Title>Add new to-do</Modal.Title>
            </Modal.Header>
            <Modal.Body> <input className='w-100' onChange={() => handleNewTodo(event.target.value)} type="text" name="" id="" /> </Modal.Body>
            <Modal.Footer>
              <Button variant="primary" onClick={handletododata}>add</Button>
            </Modal.Footer>
          </Modal>
        </>
      }
    </div>
  )
}