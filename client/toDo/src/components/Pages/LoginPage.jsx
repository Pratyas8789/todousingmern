import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
import Button from 'react-bootstrap/Button';
import { useDispatch, useSelector } from 'react-redux'
import { handleTodoAccess, handleUserName, handleUserPassword } from '../../Redux/Reducer/userReducer';

const API = "http://localhost:3007/user/login"

export default function LoginPage() {
    const navigate = useNavigate()
    const { userName, password } = useSelector(store => store.userData)

    const dispatch = useDispatch()

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (!userName || !password) {
            alert("username and password are required")
        } else {
            fetch(API, {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    userName: userName,
                    password: password
                })
            })
                .then((data) => data.json())
                .then((data) => {
                    console.log(data);
                    localStorage.setItem("token", ("Bearer " + data.token));
                    if (data.message === "success") {
                        alert("login successfully!!")
                        dispatch(handleTodoAccess())
                        navigate(`/todo/${data.id}`)
                    } else {
                        alert("please enter correct loginid or password!!! ")
                    }
                })
                .catch((err) => {
                    console.log(err.message);
                })
        }
    }
    const handleName = (name) => {
        dispatch(handleUserName(name))
    }
    const handlePassword = (password) => {
        dispatch(handleUserPassword(password))
    }

    return (
        <div className='bg-white m-5 p-5 rounded h-50 shadow-lg' >
            <h1>Welcome to To-Do application</h1>
            <form action="">
                <h3>Login id</h3>
                <input type="text" onChange={() => handleName(event.target.value)} placeholder='please enter a vaild email id' />
                <h3>Password</h3>
                <input type="password" onChange={() => handlePassword(event.target.value)} name="" id="" placeholder='please enter password' />
                <br />
                <Button className='mt-2' onClick={handleSubmit} variant="primary" type='submit' >Login</Button>{' '}
            </form>
            <hr />
            <div className='d-flex justify-content-between' >
                <p>If you are not already login</p>
                <Link to={'/'} >
                    <Button variant="primary">Signup</Button>{' '}
                </Link>
            </div>
        </div>
    )
}