import React from 'react'
import Button from 'react-bootstrap/Button';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import { handleClear, handleUserName, handleUserPassword } from '../../Redux/Reducer/userReducer';
const API = "http://localhost:3007/user"

export default function SignupPage() {
    const { userName, password } = useSelector(store => store.userData)
    const dispatch = useDispatch()
    const navigate = useNavigate()

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (!userName || !password) {
            alert("username and password are required")
        } else {
            fetch(API, {
                method: "POST",
                headers: {
                    "Content-type": "application/json"
                },
                body: JSON.stringify({
                    userName: userName,
                    password: password
                })
            })
                .then((res) => res.json())
                .then((data) => {
                    if (data.message === "user already exist") {
                        alert("user already exist")
                        dispatch(handleClear())
                    } else {
                        alert("Signup successfully!!")
                        navigate(`/todo/${data.id}`)
                    }
                })
                .catch((err) => {
                    console.log(err);
                })
        }
    }
    const handleName = (name) => {
        dispatch(handleUserName(name))
    }
    const handlePassword = (password) => {
        dispatch(handleUserPassword(password))
    }
    return (
        <div className='bg-white m-5 p-5 rounded h-50 shadow-lg'>
            <h1>Welcome to To-Do application</h1>
            <form action="">
                <h3>Email id</h3>
                <input onChange={() => handleName(event.target.value)} type="text" placeholder='please enter a vaild email id' />
                <h3>Password</h3>
                <input onChange={() => handlePassword(event.target.value)} type="password" name="" id="" placeholder='please enter password' />
                <br />
                <Button className='mt-2' onClick={handleSubmit} variant="primary" type='submit' >Signup</Button>{' '}
            </form>
            <hr />
            <div className='d-flex justify-content-between' >
                <p>If you are already login</p>
                <Link to={'/login'} >
                    <Button variant="primary">login</Button>{' '}
                </Link>
            </div>
        </div>
    )
}