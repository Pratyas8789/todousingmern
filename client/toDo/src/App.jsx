import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import SignupPage from './components/Pages/SignupPage';
import LoginPage from './components/Pages/LoginPage';
import ToDo from './components/ToDo';

function App() {
  return (
    <div className='App d-flex justify-content-center' >
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<SignupPage/>} />
          <Route path='/login' element={<LoginPage/>} />
          <Route path='/todo/:id' element={<ToDo/>} />
          <Route path='/*' element={<h1>Page Not Found</h1>} />
        </Routes>
      </BrowserRouter>

    </div>
  )
}

export default App
