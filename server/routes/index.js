var express = require('express');
var router = express.Router();
const Todo = require('../model/todoSchema')
const UserDetaills = require('../model/userDetailsSchema')
const bcrypt = require('bcryptjs');
const generateToken = require('../controller/token');

const protect = require('../middleware/authMiddleware')


router.get('/', async (req, res, next) => {
  try {
    const userDetaills = await UserDetaills.find()
    res.status(200).json({ "message": userDetaills })
  } catch (err) {
    res.status(400).json({ "Error": err.message })
  }
});

router.get('/todos',protect , async (req, res, next) => {
  try {
    // const {_id, userId} = req.
    console.log(req.user);
    const userTodos = await Todo.find()
    res.status(200).json({ "userTodos": userTodos })
  } catch (err) {
    res.status(400).json({ "Error": err.message })
  }
});

router.get('/todos/:id',protect , async (req, res, next) => {
  try {
    // console.log(req);

    const userTodos = await Todo.find({ "userId": req.params.id })
    res.status(200).json({ "todos": userTodos })
  } catch (err) {
    res.status(400).json({ "Error": err.message })
  }
});

router.get('/todos/date/:id/:date'
,protect 
, async (req, res, next) => {
  try {
    console.log("edrcfrc");
    const { id, date } = req.params
    const userTodos = await Todo.find({ "userId": id })
    if (date == 0) {
      res.status(200).json({ "filterTodo": userTodos })
    } else {
      let filterTodo = userTodos.filter((eachTodo) => eachTodo.createdAt.getDate() == date)
      res.status(200).json({ "filterTodo": filterTodo })
    }
  } catch (err) {
    res.status(400).json({ "Error": err.message })
  }
});

router.get('/todos/:id/:name',protect , async (req, res, next) => {
  try {
    const { id, name } = req.params
    const userTodos = await Todo.find({ "userId": id })
    let filterTodo = userTodos.filter((eachTodo) => eachTodo.text.includes(name))
    res.status(200).json({ "filterTodo": filterTodo })
  } catch (err) {
    res.status(400).json({ "Error": err.message })
  }
});

router.post('/todos',protect , async (req, res, next) => {
  try {
    let { text, userId } = req.body
    const newTodo = new Todo({
      text: text,
      userId: userId
    })
    newTodo.save()
    res.status(200).json({ "newTodo": newTodo })
  } catch (error) {
    res.status(400).json({ "Error": error })
  }
});

router.post('/', async (req, res, next) => {
  try {
    let { userName, password } = req.body
    if (!userName || !password) {
      res.status(400).json({ message: "username and password is required" })
    }
    else {
      const userFind = await UserDetaills.findOne({ "userName": userName })
      if (userFind) {
        res.status(200).json({ "message": "user already exist" })
      }
      else {
        const salt = await bcrypt.genSalt(10)
        const hassedPassword = await bcrypt.hash(password, salt)
        password = hassedPassword
        const newUser = new UserDetaills({
          "userName": userName,
          "password": password
        })
        newUser.save()
        res.status(200).json({ "message": newUser,
         "id": newUser._id,
         "token":generateToken(newUser._id)
         })
      }
    }
  }
  catch (error) {
    res.status(400).json({ "Error": error })
  }

})

router.post('/login', async (req, res, next) => {
  try {
    const { userName, password } = req.body
    if (!userName && !password) {
      res.status(400).json({ message: "username and password is required" })
    }
    else {
      const userFind = await UserDetaills.findOne({ userName })
      if (userFind && (await bcrypt.compare(password, userFind.password))) {        
        res.status(200).json({ "message": "success",
         "id": userFind._id ,
         "token":generateToken(userFind._id)})
      } else {
        res.status(400).json({ "message": "not success" })
      }
    }
  } catch (error) {
    res.status(400).json({ "Error": error.message })
  }
})

router.delete('/todos/delete/:id',protect , async (req, res, next) => {
  try {
    const todo = await Todo.findByIdAndDelete(req.params.id)
    res.status(200).json({ "todo": todo })
  }
  catch (error) {
    res.status(400).json({ "Error": `Id is not valid or ${error.message}` })
  }
})

router.delete('/delete/:id',protect , async (req, res, next) => {
  try {
    const userdetail = await UserDetaills.findByIdAndDelete(req.params.id)
    res.status(200).json({ "userdetail": userdetail })
  } catch (error) {
    res.status(400).json({ message: "id is not defined" })
  }
})
module.exports = router;