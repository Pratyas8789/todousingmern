const jwt = require('jsonwebtoken')

const userDetails = require('../model/userDetailsSchema')
const protect = async(req,res,next)=>{
    let token
    if(req.headers.authorization && req.headers.authorization.startsWith("Bearer")){
        try{
            token=req.headers.authorization.split(" ")[1]
            const decode = jwt.verify(token, process.env.SECRET_URI)
            req.user=await userDetails.findById(decode.id).select('-password')
            next()
        }
        catch(error){
            console.log(error.message);
            res.status(401).json({"Error":"Not authorized"})
        }
    }
    if(!token){
        res.status(401).json({"Error":"Not token"})
    }
}
module.exports=protect