const mongoose = require('mongoose')
const schema = mongoose.Schema

const todoSchema = new schema({
    userId:{
        type:String,
        required:true
    },
    text:{
        type:String,
        required:true
    }
},{collection:"todoDetails", timestamps:true})

const Todo = mongoose.model("todoDetails", todoSchema)

module.exports = Todo