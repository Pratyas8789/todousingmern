const mongoose = require('mongoose')
const schema = mongoose.Schema

const userrDetailsSchema = new schema({
    userName:{
        type:String,
        required:true
    },
    password:{
        type:String,
        required:true
    }
},{collection:"userDetails"})

const UserDetaills = mongoose.model("userDetails", userrDetailsSchema)

module.exports = UserDetaills